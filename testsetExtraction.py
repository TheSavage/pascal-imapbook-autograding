import csv

testIDs = []
with open("testSetIDs.txt") as f:
    for line in f:
        testIDs.append(line.strip())


with open("Weightless_dataset_testset", "w", encoding="utf-8", newline="") as outfile:


    with open("Weightless_dataset_train_withoutTrainA", encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        for row in reader:
            if row["ID"] in testIDs:
                #print(row)
                writer.writerow(row)



with open("Weightless_dataset_train_modelBC.csv", "w", encoding="utf-8", newline="") as outfile:
    with open("Weightless_dataset_train.csv", encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        for row in reader:
            if row["ID"] not in testIDs:
                #print(row)
                writer.writerow(row)