# pascal-imapbook-autograding

This comes with prebuild models

required python packages are listed in requirements.txt
server runs on port 8080


command to run server:

python runserver.py


ask server anything, example curl command

curl -H "Content-Type: application/json" -d '{"question" : "What is happening in the tunnel?", "modelId" :"A", "questionResponse":"Water started to come out of the top to clean the grease off the tunnel walls."}' -X POST http://localhost:8080/predict