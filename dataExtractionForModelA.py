import csv

with open("Weightless_dataset_train_modelA", "w", encoding="utf-8", newline="") as outfile:


    with open("Weightless_dataset_train.csv", encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        row_ids = ["20", "103", "187", "291", "369", "437", "499", "578", "663", "736", "809", "892"]
        for row in reader:
            if row["ID"] in row_ids:
                #print(row)
                writer.writerow(row)


with open("Weightless_dataset_train_withoutTrainA", "w", encoding="utf-8", newline="") as outfile:
    with open("Weightless_dataset_train.csv", encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        row_ids = ["20", "103", "187", "291", "369", "437", "499", "578", "663", "736", "809", "892"]
        for row in reader:
            if row["ID"] not in row_ids:
                #print(row)
                writer.writerow(row)


