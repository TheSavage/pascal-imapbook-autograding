from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from flask import Flask, json, request
import hashlib
import csv


app = Flask(__name__)

answers_to_questionsModelA = {}
text_usedModelA = {}
ratingModelA = {}

ATR_of_questionsModelB = {}

def get_cosine_sim(*strs):
    vectors = [t for t in get_vectors(*strs)]
    return cosine_similarity(vectors)


def get_vectors(*strs):
    text = [t for t in strs]
    vectorizer = CountVectorizer(text)
    vectorizer.fit(text)
    return vectorizer.transform(text).toarray()

def loadDataModelA():
    with open('Weightless_dataset_train_A.csv', encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            hash_object = hashlib.sha256(row['Question'].encode("utf-8"))
            hex_dig = hash_object.hexdigest()
            answers_to_questionsModelA[hex_dig] = row['Response']
            text_usedModelA[hex_dig] = row['Text.used.to.make.inference']
            ratingModelA[hex_dig] = row['Final.rating']
            #print(row)

def ModelA(question, question_response):
    question = question
    question_resp = question_response
    hash_object = hashlib.sha256(question.encode("utf-8"))
    hex_dig = hash_object.hexdigest()

    similarity_jac = jaccard(question_resp, answers_to_questionsModelA[hex_dig])
    similarity_cosine = get_cosine_sim(question_resp, answers_to_questionsModelA[hex_dig])
    print(similarity_cosine[1][0], similarity_jac)
    score = similarity_jac
    if score > 0.8:
        score = 1.0
    elif score > 0.4:
        score = 0.5
    else:
        score = 0

    return score

def response_string(score, probability=0.0):
    string = '{"score": sc_num, "probability": pb}'.replace("sc_num", str(score)).replace("pb", str(probability))
    return string

def jaccard(str1, str2):
    str1 = set(str1.split())
    str2 = set(str2.split())
    return float(len(str1 & str2)) / len(str1 | str2)

#request example via curl
# curl -H "Content-Type: application/json" -d '{"question" : "What is happening in the tunnel?", "modelId" :"A", "questionResponse":"Water started"}' -X POST http://localhost:5000/predict

# {"modelId":"A", "question":"How does Shiranna feel?", "questionResponse":"She is a little bit nervous."}

def ModelB(question, question_resp):
    pass


@app.route("/predict", methods=['POST'])
def prediction_model():
    data = request.get_json()
    model = data["modelId"]
    question = data["question"]
    question_resp = data["questionResponse"]

    if model == "A":
        return response_string(ModelA(question, question_resp))
    if model == "B":
        return response_string(ModelB(question, question_resp))

if __name__ == "__main__":
    loadDataModelA()
    app.run(debug=True, port=5000)
