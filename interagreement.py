import csv

N = 0.0
rater1 = 0.0
rater2 = 0.0
inter = 0.0

with open('Weightless_dataset_train.csv', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        N += 1
        if row["Glenn.s.rating"] == row["Final.rating"]:
            rater1 += 1
        if row["Amber.s.rating"] == row["Final.rating"]:
            rater2 += 1
        if row["Glenn.s.rating"] == row["Amber.s.rating"]:
            inter += 1

        print(row["Glenn.s.rating"].replace(",", "."), row["Amber.s.rating"].replace(",", "."))


print("Inter-rater agreement: "+str(inter/N), "Glenns score w.r.t to final: "+str(rater1/N), "Ambers score w.r.t to final: "+str(rater2/N))
