import csv

questions = {}

with open('Weightless_dataset_testset', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',')
    for row in reader:
        q = row["Question"]
        if q not in questions:
            questions[q] = {"0,0": 0, "0,5":0, "1,0":0}
        else:
            questions[q][row['Final.rating']] += 1


for quest in questions:
    print(quest, questions[quest]["0,0"], questions[quest]["0,5"],questions[quest]["1,0"], questions[quest]["0,0"]+questions[quest]["0,5"]+questions[quest]["1,0"])

for quest in questions:
    print(quest+" & "+str(questions[quest]["0,0"])+" & "+str(questions[quest]["0,5"])+" & "+str(questions[quest]["1,0"])+" & "+str(questions[quest]["0,0"]+questions[quest]["0,5"]+questions[quest]["1,0"])+"\\"+"\\"+" \hline")

questions = {}
print("#############################")
with open('Weightless_dataset_train_modelBC.csv', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',')
    for row in reader:
        q = row["Question"]
        if q not in questions:
            questions[q] = {"0,0": 0, "0,5":0, "1,0":0}
        else:
            questions[q][row['Final.rating']] += 1


for quest in questions:
    print(quest, questions[quest]["0,0"], questions[quest]["0,5"],questions[quest]["1,0"], questions[quest]["0,0"]+questions[quest]["0,5"]+questions[quest]["1,0"])

for quest in questions:
    print(quest+" & "+str(questions[quest]["0,0"])+" & "+str(questions[quest]["0,5"])+" & "+str(questions[quest]["1,0"])+" & "+str(questions[quest]["0,0"]+questions[quest]["0,5"]+questions[quest]["1,0"])+"\\"+"\\"+" \hline")