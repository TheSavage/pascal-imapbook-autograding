from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from flask import Flask, json, request
import hashlib
import csv
from sklearn.metrics import f1_score
from sklearn.tree import DecisionTreeClassifier
import pickle
from nltk.corpus import wordnet
from nltk.stem.porter import PorterStemmer
import string

porter_stemmer = PorterStemmer()
app = Flask(__name__)
answers_to_questionsModelA = {}
text_usedModelA = {}
ratingModelA = {}
answers_to_questionsModelBC = {}

def preproces(stra):
    stra = stra.lower()
    table = stra.maketrans({key: None for key in string.punctuation})
    text = stra.translate(table)
    return text

def common_words(s1, s2):
    s1 = preproces(s1)
    s2 = preproces(s2)

    s1 = set(s1.split())

    common = 0.0
    for el in s1:
        if el in s2:
            common += 1

    return (2*common)/(len(s1)+len(set(s2.split())))

def common_words_with_syns_similarity(to_syns, ref):
    to_syns = preproces(to_syns)
    ref = preproces(ref)
    sen_with_syns = []
    for el in to_syns.split():
        #print(el)
        newset = set()
        newset.add(porter_stemmer.stem(el))
        for syn in wordnet.synsets(el):
            for lm in syn.lemmas():
                #print(porter_stemmer.stem(lm.name()))
                if "_" not in porter_stemmer.stem(lm.name()):
                    newset.add(porter_stemmer.stem(lm.name()))
        sen_with_syns.append(newset)

    ref_splited = ref.split()
    for i in range(0, len(ref_splited)):
        ref_splited[i] = porter_stemmer.stem(ref_splited[i])

    ref = " ".join(ref_splited)

    common_word = 0
    for el in sen_with_syns:
        for el2 in el:
            if el2 in ref:
                common_word += 1

    if common_word > len(sen_with_syns):
        common_word = len(sen_with_syns)
    return float(2*common_word)/(len(ref_splited)+len(to_syns.split()))


# For cosine similarity; bag of words with normalization for vector inputs
#https://towardsdatascience.com/overview-of-text-similarity-metrics-3397c4601f50
def get_cosine_sim(*strs):
    vectors = [t for t in get_vectors(*strs)]
    return cosine_similarity(vectors)
def get_vectors(*strs):
    text = [t for t in strs]
    vectorizer = CountVectorizer(text)
    vectorizer.fit(text)
    return vectorizer.transform(text).toarray()

def loadDataModelA():
    with open('Weightless_dataset_train_modelA', encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        for row in reader:
            hash_object = hashlib.sha256(row['Question'].encode("utf-8"))
            hex_dig = hash_object.hexdigest()
            print(row['Question'], hex_dig)
            answers_to_questionsModelA[hex_dig] = row['Response']
            text_usedModelA[hex_dig] = row['Text.used.to.make.inference']
            ratingModelA[hex_dig] = row['Final.rating']


def loadDataModelBC():
    with open('Weightless_dataset_train_modelBC.csv', encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        for row in reader:
            hash_object = hashlib.sha256(row['Question'].encode("utf-8"))
            hex_dig = hash_object.hexdigest()
            print(row['Question'], hex_dig)
            if hex_dig in answers_to_questionsModelBC:
                answers_to_questionsModelBC[hex_dig].append([row['Response'], row['Text.used.to.make.inference'], row['Final.rating']])
            else:
                answers_to_questionsModelBC[hex_dig] = []
                answers_to_questionsModelBC[hex_dig].append([row['Response'], row['Text.used.to.make.inference'], row['Final.rating']])


answithrate1 = {}
featureTable = []
X_trainB = []
y_trainB = []
X_trainC = []
y_trainC = []
def FitModelB():
    clf = DecisionTreeClassifier()
    clf.fit(X_trainB, y_trainB)
    return clf

def FitModelC():
    clfC = DecisionTreeClassifier(min_samples_leaf=7)
    clfC.fit(X_trainC, y_trainC)
    return clfC

clf = None
clfC = None

# For longest common subsequence
#https://rosettacode.org/wiki/Longest_common_subsequence#Python
def lcs(a, b):
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = max(lengths[i+1][j], lengths[i][j+1])
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + result
            x -= 1
            y -= 1
    return result

def ModelBBuild():

    for el in answers_to_questionsModelBC:
        answithrate1[el] = []
        for i in range(0, len(answers_to_questionsModelBC[el])):
            if float(answers_to_questionsModelBC[el][i][2].replace(",", ".")) == 1:
                answithrate1[el].append(answers_to_questionsModelBC[el][i][0])


    for el in answers_to_questionsModelBC:
        for i in range(0, len(answers_to_questionsModelBC[el])):
            truth = float(answers_to_questionsModelBC[el][i][2].replace(",", "."))
            respo = answers_to_questionsModelBC[el][i][0]
            max_sim = 0.0
            max_lcs = 0.0
            max_common = 0.0
            for j in range(0, len(answithrate1[el])):
                answa = answithrate1[el][j]
                if hashlib.sha256(respo.encode("utf-8")).digest() == hashlib.sha256(answithrate1[el][j].encode("utf-8")).digest():
                    continue
                lcomsu = 2*len(lcs(preproces(respo), preproces(answa)))/(len(preproces(respo))+len(preproces(answa)))
                cos_sim = get_cosine_sim(respo, answa)[1][0]
               # commono = common_words(respo, answa)
                if cos_sim > max_sim:
                    max_sim = cos_sim
                if lcomsu > max_lcs:
                    max_lcs = lcomsu
                # if commono > max_common:
                #     max_common = commono
                #print(j)
            featureTable.append([max_sim, truth])
            X_trainB.append([max_sim, max_lcs])
            y_trainB.append(int(truth*10))
            print([max_sim, max_lcs, truth])


def FeatureTest():

    for el in answers_to_questionsModelBC:
        answithrate1[el] = []
        for i in range(0, len(answers_to_questionsModelBC[el])):
            if float(answers_to_questionsModelBC[el][i][2].replace(",", ".")) == 1:
                answithrate1[el].append(answers_to_questionsModelBC[el][i][0])


    for el in answers_to_questionsModelBC:
        for i in range(0, len(answers_to_questionsModelBC[el])):
            truth = float(answers_to_questionsModelBC[el][i][2].replace(",", "."))
            respo = answers_to_questionsModelBC[el][i][0]
            max_sim = 0.0
            anss = ""
            ana = ""
            for j in range(0, len(answithrate1[el])):
                answa = answithrate1[el][j]
                if hashlib.sha256(respo.encode("utf-8")).digest() == hashlib.sha256(answithrate1[el][j].encode("utf-8")).digest():
                    continue
                #commono = diffSim(respo, answa)
                commono = (common_words_with_syns_similarity(respo, answa)+common_words_with_syns_similarity(respo, answa))/2.0
                #print(commono, answa, respo)
                if commono > max_sim:
                    max_sim = commono
                    anss = answithrate1[el][j]
                    ana = answa
                #print(j)
            print(max_sim, truth)


def ModelCBuild():

    for el in answers_to_questionsModelBC:
        answithrate1[el] = []
        for i in range(0, len(answers_to_questionsModelBC[el])):
            if float(answers_to_questionsModelBC[el][i][2].replace(",", ".")) == 1:
                answithrate1[el].append(answers_to_questionsModelBC[el][i][0])


    for el in answers_to_questionsModelBC:
        for i in range(0, len(answers_to_questionsModelBC[el])):
            truth = float(answers_to_questionsModelBC[el][i][2].replace(",", "."))
            respo = answers_to_questionsModelBC[el][i][0]

            max_sim = 0.0
            max_cow_syn_sim = 0.0
            max_lcs = 0.0
            numba = 0.0
            for j in range(0, len(answithrate1[el])):
                numba += 1
                if hashlib.sha256(respo.encode("utf-8")).digest() == hashlib.sha256(answithrate1[el][j].encode("utf-8")).digest():
                    continue

                cos_sim = get_cosine_sim(respo, answithrate1[el][j])[1][0]
                cow_syn_sim = (common_words_with_syns_similarity(respo, answithrate1[el][j])+common_words_with_syns_similarity(respo, answithrate1[el][j]))/2.0
                lcomsu = 2 * len(lcs(preproces(respo), preproces(answithrate1[el][j]))) / (
                            len(preproces(respo)) + len(preproces(answithrate1[el][j])))
                if cos_sim > max_sim:
                    max_sim = cos_sim
                if lcomsu > max_lcs:
                    max_lcs = lcomsu
                if cow_syn_sim > max_cow_syn_sim:
                   max_cow_syn_sim = cow_syn_sim
            featureTable.append([max_sim, truth])
            X_trainC.append([max_sim, max_lcs, max_cow_syn_sim])
            y_trainC.append(int(truth*10))
            print([max_sim, max_lcs, max_cow_syn_sim, truth])

def ModelA(question, question_response):
    question = question
    question_resp = question_response
    hash_object = hashlib.sha256(question.encode("utf-8"))
    hex_dig = hash_object.hexdigest()
    similarity_cosine = get_cosine_sim(question_resp, answers_to_questionsModelA[hex_dig])
    score = similarity_cosine[1][0]
    if score > 0.20:
        score = 1.0
    elif score > 0.05:
        score = 0.5
    else:
        score = 0

    return score

def ModelB(question, question_resp):
    hash_object = hashlib.sha256(question.encode("utf-8"))
    hex_dig = hash_object.hexdigest()
    el = hex_dig
    max_sim = 0.0
    max_lcs = 0.0
    max_common = 0.0
    for i in range(0, len(answers_to_questionsModelBC[el])):
        answa = answers_to_questionsModelBC[el][i][0]
        cos_sim = get_cosine_sim(question_resp, answa)[1][0]
        lcomsu = 2*len(lcs(preproces(question_resp), preproces(answa)))/(len(preproces(question_resp))+len(preproces(answa)))
        commono = common_words(question_resp, answa)
        if cos_sim > max_sim:
            max_sim = cos_sim
        if lcomsu > max_lcs:
            max_lcs = lcomsu
    return clf.predict([[max_sim, max_lcs]])/10.0

def ModelC(question, question_resp):
    hash_object = hashlib.sha256(question.encode("utf-8"))
    hex_dig = hash_object.hexdigest()
    el = hex_dig
    max_sim = 0.0
    max_cow_syn_sim = 0.0
    max_lcs = 0.0
    for i in range(0, len(answers_to_questionsModelBC[el])):
        cow_syn_sim = (common_words_with_syns_similarity(question_resp, answers_to_questionsModelBC[el][i][0])+common_words_with_syns_similarity(question_resp, answers_to_questionsModelBC[el][i][0]))/2.0
        cos_sim = get_cosine_sim(question_resp, answers_to_questionsModelBC[el][i][0])[1][0]
        lcomsu = 2 * len(lcs(preproces(question_resp), preproces(answers_to_questionsModelBC[el][i][0]))) / (len(preproces(question_resp)) + len(preproces(answers_to_questionsModelBC[el][i][0])))
        if cos_sim > max_sim:
            max_sim = cos_sim
        if lcomsu > max_lcs:
            max_lcs = lcomsu
        if cow_syn_sim > max_cow_syn_sim:
            max_cow_syn_sim = cow_syn_sim

    return clfC.predict([[max_sim, max_lcs, max_cow_syn_sim]])[0]/10.0

def response_string(score, probability=0.0):
    string = '{"score": sc_num, "probability": pb}'.replace("sc_num", str(score)).replace("pb", str(probability))
    return string


def prediction_model():
    data = request.get_json()
    model = data["modelId"]
    question = data["question"]
    question_resp = data["questionResponse"]

    if model == "A":
        return (ModelA(question, question_resp))
    if model == "B":
        return (ModelB(question, question_resp))
    if model == "C":
        return (ModelC(question, question_resp))


correct = 0.0
all = 0
loadDataModelA()
loadDataModelBC()

if False:
    FeatureTest()
    quit()
try:
    infile = open("featureTable.dat", 'rb')
    X_trainB = pickle.load(infile)
    infile.close()

    infile = open("featureTableTruth.dat", 'rb')
    y_trainB = pickle.load(infile)
    infile.close()
except Exception:
    ModelBBuild()
    outfile = open("featureTable.dat", 'wb')
    pickle.dump(X_trainB, outfile)
    outfile.close()
    outfile = open("featureTableTruth.dat", 'wb')
    pickle.dump(y_trainB, outfile)
    outfile.close()


try:
    infile = open("featureTableC.dat", 'rb')
    X_trainC = pickle.load(infile)

    infile.close()
    infile = open("featureTableTruthC.dat", 'rb')
    y_trainC = pickle.load(infile)
    infile.close()
except Exception:
    ModelCBuild()
    outfile = open("featureTableC.dat", 'wb')
    pickle.dump(X_trainC, outfile)
    outfile.close()
    outfile = open("featureTableTruthC.dat", 'wb')
    pickle.dump(y_trainC, outfile)
    outfile.close()



clf = FitModelB()
clfC = FitModelC()
y_true = []
yA_pred = []
yB_pred = []
yC_pred = []

with open('Weightless_dataset_testset', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',')
    for row in reader:
        all += 1
        print(row['Question'])
        gradeA = ModelA(row['Question'], row['Response'])
        gradeB = ModelB(row['Question'], row['Response'])
        gradeC = ModelC(row['Question'], row['Response'])
        yA_pred.append(int(gradeA*10))
        yB_pred.append(int(gradeB*10))
        yC_pred.append(int(gradeC*10))
        y_true.append(int(float(row['Final.rating'].replace(",", "."))*10))


print("FmacroA: ", f1_score(y_true, yA_pred, average='macro'))
print("FmicroA: ", f1_score(y_true, yA_pred, average='micro'))

print("FmacroB: ", f1_score(y_true, yB_pred, average='macro'))
print("FmicroB: ", f1_score(y_true, yB_pred, average='micro'))

print("FmacroC: ", f1_score(y_true, yC_pred, average='macro'))
print("FmicroC: ", f1_score(y_true, yC_pred, average='micro'))

