from nltk.corpus import stopwords
import string
import nltk
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn
porter_stemmer = PorterStemmer()

lemmatizer = WordNetLemmatizer()

s1 = "She is excited but scared at the same time."
s2 = "like she can do it !"


def preproces(str):
    str = str.lower()
    table = str.maketrans({key: None for key in string.punctuation})
    text = str.translate(table)
    return text


def common_words_with_syns_similarity(to_syns, ref):
    to_syns = preproces(to_syns)
    ref = preproces(ref)
    sen_with_syns = []
    for el in to_syns.split():
        #print(el)
        newset = set()
        newset.add(porter_stemmer.stem(el))
        for syn in wordnet.synsets(el):
            for lm in syn.lemmas():
                #print(porter_stemmer.stem(lm.name()))
                if "_" not in porter_stemmer.stem(lm.name()):
                    newset.add(porter_stemmer.stem(lm.name()))
        sen_with_syns.append(newset)

    ref_splited = ref.split()
    for i in range(0, len(ref_splited)):
        ref_splited[i] = porter_stemmer.stem(ref_splited[i])

    ref = " ".join(ref_splited)

    common_word = 0
    for el in sen_with_syns:
        for el2 in el:
            if el2 in ref:
                common_word += 1
    if common_word > len(sen_with_syns):
        common_word = len(sen_with_syns)
    #print(common_word)
    return float(2*common_word)/(len(ref_splited)+len(to_syns.split()))


print(common_words_with_syns_similarity(s2, s1))
print(common_words_with_syns_similarity(s1, s2))

def common_words_with_syns_similarity(var1, var2):
    var1 = preproces(var1)
    var2 = preproces(var2)
    var1_syns = set()
    var2_syns = set()
    for el in var1.split():
        var1_syns.add(porter_stemmer.stem(el))
        for syn in wordnet.synsets(el):
            for lm in syn.lemmas():
                #print(porter_stemmer.stem(lm.name()))
                if "_" not in porter_stemmer.stem(lm.name()):
                    var1_syns.add(porter_stemmer.stem(lm.name()))

    for el in var2.split():
        var2_syns.add(porter_stemmer.stem(el))
        for syn in wordnet.synsets(el):
            for lm in syn.lemmas():
                #print(porter_stemmer.stem(lm.name()))
                if "_" not in porter_stemmer.stem(lm.name()):
                    var2_syns.add(porter_stemmer.stem(lm.name()))

    common_word = 0.0
    for el in var1_syns:
        if el in var2_syns:
            common_word += 1

    #print(common_word)
    return float(2*common_word)/(len(var1_syns)+len(var2_syns))


print(common_words_with_syns_similarity(s2, s1))
print(common_words_with_syns_similarity(s1, s2))






# print(wordnet_synonyms)
# # print(set1)
# # print(sen_with_syns)
# print(preproces(sen))

