from sklearn.linear_model import LogisticRegression

X = [[0.8780487804878049, 0.86625],[0.8148148148148148, 0.7230218855218855],[0.46153846153846156, 0.7636041926483104]]
Y = [10, 0, 5]

clf = LogisticRegression(multi_class='multinomial', solver="saga")

clf.fit(X, Y)

print(clf.predict([[0.8148148148148148, 0.7230218855218855]]))
